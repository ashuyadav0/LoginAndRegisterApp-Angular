<h1>Login and Register App</h1>
This login and registration application is built with Angular on the frontend, Node.js and Express on the backend, and a MySQL database to store the data of registered users.

<h2>Requirements</h2>
Before you start working with the app, make sure you have the following installed:

<li>Angular CLI</li>
<li>Node.js</li>
<li>MySQL</li>
<li>Express</li>
<li>Cors</li>

<h2>Gallery</h2>
<img src="homePageWithoutLogin.png">
<img src="loginPage.png">
<img src="registerPage.png">
<img src="homePage.png">

<h2>Parameters</h2>
To start the project we execute the following:

Server: 
```bash
npm start
```
The client will request this url.
| URL            | PORT |
|----------------|------|
| localhost:4000 | 4000 |

Client: 
```bash
npm start
```
| URL            | PORT |
|----------------|------|
| localhost:4200 | 4200 |
