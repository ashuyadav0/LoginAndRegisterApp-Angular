import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import Swal from 'sweetalert2';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public loginForm!: FormGroup;
    constructor(private authenticationService: AuthenticationService) { }
    ngOnInit() {
        this.loginForm = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
        });

        if(this.authenticationService.isLoggedIn()){
           location.href = '/';
        }
    }
    public onSubmit() {
        if (this.loginForm.valid) {
            this.authenticationService.login(
                this.loginForm.get('username')!.value,
                this.loginForm.get('username')!.value,
                this.loginForm.get('password')!.value
            );
        } else{
            Swal.fire('Error', 'The username and password fields are required!', 'error');
            console.log('Err!, No se puede iniciar sessión porque los cmapos estan vacios!')
        }
    }
}