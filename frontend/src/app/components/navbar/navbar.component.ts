import { Component } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  showNavbar: boolean = false;
  showProfileDropdown: boolean = false;
  constructor(public authService: AuthenticationService) {
  }

  toggleNavbar() {
    this.showNavbar = !this.showNavbar;
  }
}
