import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { LoginComponent } from '../components/login/login.component';
import { PagenotfoundComponent } from '../components/pagenotfound/pagenotfound.component';
import { PrivacyPolicyComponent } from '../components/privacy-policy/privacy-policy.component';
import { ProfileComponent } from '../components/profile/profile.component';
import { RegisterComponent } from '../components/register/register.component';
import { AuthGuard } from '../helpers/auth.guard';
import { LandingPageComponent } from '../components/landing-page/landing-page.component';

const routes: Routes = [
  { path: 'home', canActivate: [AuthGuard], component: HomeComponent},
  { path: '', component: LandingPageComponent},
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: '**', component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutesRoutingModule { }
