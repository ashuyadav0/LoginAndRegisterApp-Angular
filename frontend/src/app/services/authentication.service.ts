import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { AuthenticationClient } from '../clients/authentication.client';

const USERNAME_KEY = 'username';
const EMAIL_KEY = 'email';
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private tokenKey = 'token';

  constructor(
    private authenticationClient: AuthenticationClient,
    private router: Router
  ) {}

  public login(username: string, email: string, password: string): void {
    this.authenticationClient.login(username, password).subscribe((response) => {
      const responseObject = JSON.parse(response);
      if (responseObject.status === 1 && responseObject.token) {
        localStorage.setItem(this.tokenKey, responseObject.token);
        localStorage.setItem(USERNAME_KEY, username); // Guarda el nombre de usuario en localStorage
        localStorage.setItem(EMAIL_KEY, email);
        this.router.navigate(['/']);
        console.log('Has iniciado sesión con el usuario: ' + username);
      } else {
        Swal.fire('Error' ,'Username o Password incorrerts.', 'error');
        console.log('Usuario o contraseña incorrectos.');
      }
    });
  }

  public register(fullname: string, username: string, email: string, password: string): void {
    // Register the user if they do not exist
    this.authenticationClient
        .register(fullname, username, email, password)
        .subscribe((token) => {
            localStorage.setItem(this.tokenKey, token);
            this.router.navigate(['/']);
            Swal.fire('Success', 'The registration has been successful!!', 'success');
            console.log('El registro se ha hecho correctamente!');
        });
        localStorage.setItem(USERNAME_KEY, username);
        localStorage.setItem(EMAIL_KEY, email);
        if(this.tokenKey){
          Swal.fire('Error', 'The username or email address is already in use.', 'error');
        }
}

  public logout() {
    localStorage.removeItem(this.tokenKey);
    localStorage.clear();
    this.router.navigate(['/']);
    console.log('Has cerrado la session!')
  }

  public isLoggedIn(): boolean {
    let token = localStorage.getItem(this.tokenKey);
    return token != null && token.length > 0;
  }

  public get username(): any {
    return localStorage.getItem(USERNAME_KEY);
  }

  public get email(): any {
    return localStorage.getItem(EMAIL_KEY);
  }

  public getToken(): string | null {
    return this.isLoggedIn() ? localStorage.getItem(this.tokenKey) : null;
  }
}
