const express = require('express');
const router = express.Router();
const profile = require('./profile');

const user = require('./user');

router.use('/user', user);
router.use('/profile', profile);

module.exports = router;